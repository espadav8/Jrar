package espadav8.jrar.debug;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileWriter;
import java.util.Calendar;
import java.util.Date;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.ScrollPaneConstants;

import espadav8.jrar.Jrar;
import espadav8.jrar.JrarConstants;

/*
 * Copyright (C) 2003-2004 Andrew Smith
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307 USA
 */

public class DebugWindow extends JFrame implements ActionListener {

	private static JFrame df;

	private static JPanel buttonPanel;

	private static JButton clearTextArea, saveTextArea, closeDebugWindow;

	private static JScrollPane scrollPane;

	private static JTextArea textArea;

	private static Jrar jrar;

	public DebugWindow(Jrar j) {
		jrar = j;
		df = new JFrame("Debug output");

		textArea = new JTextArea("Debugging On\n", 20, 40);
		textArea.setEditable(false);

		scrollPane = new JScrollPane(textArea,
				ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS,
				ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		df.getContentPane().add("Center", scrollPane);

		buttonPanel = new JPanel();
		buttonPanel.setLayout(new BoxLayout(buttonPanel, BoxLayout.LINE_AXIS));
		clearTextArea = new JButton("Clear");
		clearTextArea.addActionListener(this);
		clearTextArea.setToolTipText("Clear the contents of the text area");
		saveTextArea = new JButton("Save output");
		saveTextArea.addActionListener(this);
		saveTextArea
				.setToolTipText("Save the output from the text area to JrarDebugOutput.txt");
		closeDebugWindow = new JButton("Stop Debuging");
		closeDebugWindow.addActionListener(this);
		closeDebugWindow
				.setToolTipText("Close this window and stops the debug output");

		buttonPanel.add(clearTextArea);
		buttonPanel.add(saveTextArea);
		buttonPanel.add(closeDebugWindow);
		df.getRootPane().setDefaultButton(saveTextArea);
		df.getContentPane().add("South", buttonPanel);

		df.pack();
		df.setLocation(0, 0);
		df.setVisible(true);
	}

	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == clearTextArea) {
			textArea.setText("");
		} else if (e.getSource() == closeDebugWindow) {
			df.dispose();
			jrar.stopDebugging();
		} else if (e.getSource() == saveTextArea) {
			Date d = new Date();
			Calendar c = Calendar.getInstance();
			String fileName = "JrarDebugOutput-" + c.get(Calendar.YEAR)
					+ "-" + c.get(Calendar.MONTH) + "-"
					+ c.get(Calendar.DAY_OF_MONTH) + ".txt";
			File f = new File(fileName);
			
			FileWriter fw = null;
			try {
				fw = new FileWriter(f, true);
				if (!f.exists())
					f.createNewFile();

				String textToWrite = "\n" + d.toString()
						+ "\n----- Start debug output -----\n"
						+ "Using version " + JrarConstants.JRARVERSION + "\n\n"
						+ textArea.getText()
						+ "------ End debug output ------\n\n";

				fw.write(textToWrite);
				fw.close();
			} catch (Exception ex) {
				addDebugOutput("Error: " + ex);
			} finally {
			   try {
			      if (fw != null)
			      fw.close();
			   } catch (Exception ex) {
			      ex.printStackTrace();
			   }
			}
		}
	}

	public void addDebugOutput(String s) {
		s = s.trim();
		textArea.append(s + "\n");
		textArea.setCaretPosition(textArea.getDocument().getLength());
	}

	public void debugFrameVisiability() {
		df.setVisible(!df.isVisible());
	}
}