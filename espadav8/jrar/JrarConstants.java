package espadav8.jrar;

import java.awt.Dimension;

/*
 * Copyright (C) 2003-2004 Andrew Smith
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307 USA
 */

public class JrarConstants {

	public static final int BUTTONSQUARESIZE = 60;

	public static final boolean DISPLAYHELP = false;

	public static final boolean MAINFRAMEISRESIZABLE = false;

	public static final boolean MAINFRAMEISVISIABLE = true;

	public static final Dimension MAINFRAMESIZE = new Dimension(790, 585);

	public static final char OVERWRITEALLANSWER = 'a';

	public static final char OVERWRITENEVERANSWER = 'e';

	public static final char OVERWRITENOANSWER = 'n';

	public static final char OVERWRITEQUITANSWER = 'q';

	public static final char OVERWRITEYESANSWER = 'y';

	public static final String JRARVERSION = "0.42";

	public static final String PROGRAMTITLE = "Jrar BETA v0.40 \'Open\',"
			+ " \'Add\' and \'Extract To\' work for all" + " archives.";

	public static final String EXTRACTIONMESSAGE = "Please wait whilest"
			+ " the files are" + " extracting. Click" + " \'Cancel\' to"
			+ " stop extraction";

	public static final String ADDINGMESSAGE = "Please wait whilest"
			+ " the new archive" + " is created";

	public static final int EXTRACTSTATE = 0;

	public static final int ADDSTATE = 1;

	public static final int DELETESTATE = 2;

	public static final int PROTECTSTATE = 3;

	public static final String[] addingPannelCompressionStrengths = {"Store",
			"Fastest", "Fast", "Normal", "Good", "Best"};

	public static final String[] addingPannelArchiveSplitSizes = {"",
			"1,457,664 - 3.5\"", "98,078k - ZIP-100", "650m - CD-650M",
			"700m - CD-700M", "Autodetect"};

	public static final double maxFileSplitingSize = 10E10 - 1;

	public static final String[] addingPannelCheckBoxOptionTexts = {
			"Delete files after archiving", "Create SFX archive",
			"Create solid archive", "Put recovery record",
			"Test archived files", "Lock archive"};

	public static final String extractionCommand = "x -ierr ";

	public static final String additionCommand = "a -ierr ";
	
	public static final String rarProgramNotFound = "???RAR_PROGRAM_NOT_FOUND???";

}