package espadav8.jrar.rar;

import javax.swing.AbstractListModel;

/*
 * Copyright (C) 2003-2004 Andrew Smith
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307 USA
 */

public class AddingListModel extends AbstractListModel {

   Object[] data = {};

   public int getSize() {
      return data.length;
   }

   public Object getElementAt(int i) {
      return data[i];
   }

   public void addNewRow(String path) {
      Object[] tempData = new Object[data.length + 1];
      for (int i = 0; i < data.length; i++) {
         tempData[i] = data[i];
      }
      tempData[data.length] = path;
      data = tempData;
   }

   public boolean checkForFile(String path) {
      boolean exists = false;

      for (int i = 0; i < data.length; i++) {
         if (data[i].equals(path))
            exists = true;
      }

      return exists;
   }

   public void removeRows(int[] rows) {

      if ((data.length - rows.length) == 0) {
         data = new Object[0];
      } else {
         Object[] tempData = new Object[data.length - rows.length];

         int j = 0;

         for (int i = 0; i < data.length; i++) {
            if (j < rows.length)
               if (rows[j] == i)
                  j++;
               else
                  tempData[i - j] = data[i];
            else
               tempData[i - j] = data[i];
         }

         data = tempData;
      }
   }
}