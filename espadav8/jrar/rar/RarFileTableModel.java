package espadav8.jrar.rar;

import javax.swing.table.AbstractTableModel;

/*
 * Copyright (C) 2003-2004 Andrew Smith
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307 USA
 */

public class RarFileTableModel extends AbstractTableModel {

	private final static String[] columnNames = {"Name", "Size", "Packed",
			"Ratio", "Date", "Time", "Attributes", "CRC32"};

	Object[][] data = {};

	// Extra data that is stored Meth, Ver, Host OS, Solid, Old
	Object[][] dataExtraInfo = {};

	Object[][] emptyData = {};

	public int getColumnCount() {
		return columnNames.length;
	}

	public int getRowCount() {
		return data.length;
	}

	public String getColumnName(int col) {
		return columnNames[col];
	}

	public Object getValueAt(int row, int col) {
		return data[row][col];
	}

	public Class getColumnClass(int c) {
		return getValueAt(0, c).getClass();
	}

	public void resetToNewData() {
		data = emptyData;
		dataExtraInfo = emptyData;
	}

	public void addNewRow(Object[] nextLine) {
		Object[][] tempData = new Object[data.length + 1][8];
		for (int i = 0; i < 8; i++) {
			for (int j = 0; j < data.length; j++) {
				tempData[j][i] = data[j][i];
			}
			tempData[data.length][i] = nextLine[i];
		}
		data = tempData;

		Object[][] tED = new Object[dataExtraInfo.length + 1][5];
		for (int i = 0; i < 5; i++) {
			for (int j = 0; j < dataExtraInfo.length; j++) {
				tED[j][i] = dataExtraInfo[j][i];
			}
			tED[dataExtraInfo.length][i] = nextLine[i + 7];
		}
		dataExtraInfo = tED;
	}
}