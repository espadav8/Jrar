package espadav8.jrar.rar;

import java.io.InputStreamReader;
import espadav8.jrar.Jrar;
import espadav8.jrar.JrarConstants;

/*
 * Copyright (C) 2003-2004 Andrew Smith
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307 USA
 */

public class AddingRarProcess extends RarProcess {
   String currentFile, switchesToUse, filesToAdd;

   boolean shouldIStop = false;

   public AddingRarProcess(String f, String stu, String fta, Jrar j) {
      currentFile = f;
      switchesToUse = stu;
      filesToAdd = fta;
      jrar = j;
   }

   public void run() {
      jrar.setJGUIButtonsState(false);
      jrar.addCancelButtonToJGUIStatusBar(this);
      jrar.updateJGUIStatusBar("" + JrarConstants.ADDINGMESSAGE);
      try {
         // set the default commmand for adding files
         String command = JrarConstants.additionCommand;
         // add the switches to the command
         command += switchesToUse;
         // add the rar file to the end of the command
         command += currentFile;
         // add the files to be added to the command
         command += filesToAdd;
         // finding the location of the EXE file
         String programLocation = jrar.findRarProgram();
         //			 if jrar.findRarProgram() returns "???RAR_PROGRAM_NOT_FOUND???"
         // then we know the file isn't around and so we can't run the
         // command.
         
         // TODO Add a return to the GUI so that the user knows that the
         // program can't be found
         if (!programLocation.equals("???RAR_PROGRAM_NOT_FOUND???")) {
            // tell people what command is being ran
            jrar.addToDebugWindow("command being run - " + programLocation
                  + command);
            // run the command
            p = Runtime.getRuntime().exec(programLocation + command);
            // start listening to the outpur from the rar process
            jrar.addToDebugWindow("Starting startInputListener");
            startInputListener(new InputStreamReader(p.getErrorStream()));
         }
      } catch (Exception e) {
         jrar.updateJGUIStatusBar("Error T: " + e);
         p.destroy();
      }
      if (!shouldIStop) {
         jrar.addToDebugWindow("Finished AddingRarProcess");
         jrar.addingRarProcessFinishedSucessfully();
      } else {
         jrar.addToDebugWindow("AddingRarProcess stopped by user");
         p.destroy();
         jrar.addingRarProcessStopped();
      }
   }

   private void startInputListener(InputStreamReader ipsr) {
      String s = "";
      jrar.addToDebugWindow("in startInputListener");
      try {
         int temp;
         do {
            temp = ipsr.read();
            System.out.print((char) temp);
            s += (char) temp;
            String[] sp = s.split("\n");
         } while ((temp != -1) && (!shouldIStop));
      } catch (Exception e) {
         jrar.addToDebugWindow("Error PSH: " + e);
      }
      if (shouldIStop) {
         jrar.addToDebugWindow("thread canceled by user");
      }
   }

   public void setStop(boolean b) {
      jrar.addToDebugWindow("setting shouldIStop to " + b);
      shouldIStop = b;
   }
}