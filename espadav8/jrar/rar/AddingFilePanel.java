package espadav8.jrar.rar;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.io.IOException;
import java.text.NumberFormat;

import javax.swing.Box;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.BevelBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;

import espadav8.jrar.Jrar;
import espadav8.jrar.JrarConstants;
import espadav8.jrar.gui.JrarGUI;

/*
 * Copyright (C) 2003-2004 Andrew Smith
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307 USA
 */

public class AddingFilePanel extends JPanel implements ActionListener,
      ItemListener {

   private Jrar jrar;
   
   private JrarGUI jgui;

   private JPanel tablePanel, addingButtons;

   private JList jal;

   private JScrollPane scrollPane;

   private JComboBox compressionMethodComboBox;

   private JFormattedTextField splitArchiveTextField;

   // options layout Delete files after archiving, create SFX archive,
   // create solid archive, put recovery record,
   // test archived files, lock archive
   private boolean[] options = { false, false, false, false, false, false };

   // other options that cannot be satisifed with a boolean
   private int currentCompressionMethod = 3;

   private int recoveryRecordSize = 0;

   private double actualSplitSize = 0;

   private JCheckBox[] optionCheckBoxes = new JCheckBox[JrarConstants.addingPannelCheckBoxOptionTexts.length];

   private JButton cancelAddingFiles, addFileButton, optionsButton, finished,
         removeFiles;

   private DefaultListModel dlm;

   private String currentFile;

   public AddingFilePanel(JPanel jp, Jrar j, String f) {
      tablePanel = jp;
      jrar = j;
      currentFile = f;

      setLayout(new BorderLayout());
      addingButtons = new JPanel();
      jal = new JList(dlm = new DefaultListModel());

      scrollPane = new JScrollPane(jal);
      scrollPane.setBorder(new BevelBorder(BevelBorder.LOWERED));
      scrollPane.setPreferredSize(new Dimension(600, 500));
      add("Center", scrollPane);

      addFileButton = new JButton("Add files");
      addFileButton.addActionListener(this);
      addFileButton.setToolTipText("Add files to the list");
      addingButtons.add(addFileButton);

      removeFiles = new JButton("Remove File(s)");
      removeFiles.addActionListener(this);
      removeFiles.setToolTipText("Remove selected file(s) from the list");
      addingButtons.add(removeFiles);

      optionsButton = new JButton("Options");
      optionsButton.addActionListener(this);
      optionsButton.setToolTipText("Options");
      addingButtons.add(optionsButton);

      finished = new JButton("Finished");
      finished.addActionListener(this);
      finished.setToolTipText("Create updated archive");
      addingButtons.add(finished);

      cancelAddingFiles = new JButton("Cancel");
      cancelAddingFiles.addActionListener(this);
      cancelAddingFiles.setToolTipText("Cancel adding files");
      addingButtons.add(cancelAddingFiles);

      this.add("South", addingButtons);
   }

   public void actionPerformed(ActionEvent e) {
      if (e.getSource() == cancelAddingFiles) {
         jrar.setJGUIButtonsState(true);
         CardLayout cl = (CardLayout) (tablePanel.getLayout());
         tablePanel.remove(this);
         cl.removeLayoutComponent(this);
         cl.last(tablePanel);
      }

      else if (e.getSource() == addFileButton) {
         addFilesChooser();
      }

      else if (e.getSource() == removeFiles) {
         removeSelectedFiles();
      }

      else if (e.getSource() == optionsButton) {
         displayOptions();
      }

      else if (e.getSource() == finished) {
         runFinishedCommand();
      }
   }

   private void addFilesChooser() {
      JFileChooser fc = new JFileChooser();
      fc.setMultiSelectionEnabled(true);
      fc.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
      int answer = fc.showDialog(this, "Add files");

      if (answer == JFileChooser.APPROVE_OPTION) {
         try {
            File[] selectedFiles = fc.getSelectedFiles();
            for (int i = 0; i < selectedFiles.length; i++) {
               if (!dlm.contains(selectedFiles[i].getCanonicalPath())) {
                  dlm.addElement(selectedFiles[i].getCanonicalPath());
               } else {
                  jrar
                        .updateJGUIStatusBar("At least one of the files you tried to add"
                              + " is already in the list");
               }
            }
         } catch (IOException e) {
            e.printStackTrace();
         }
      }
   }

   private void removeSelectedFiles() {
      int[] j = jal.getSelectedIndices();
      for (int i = (j.length - 1); i >= 0; i--)
         dlm.removeElementAt(j[i]);
   }

   private void displayOptions() {

      final JDialog optionsDialog = new JDialog(jgui, "Options", true);
      optionsDialog.getContentPane().setLayout(new BorderLayout());

      JLabel jl = new JLabel("Options for files to be added");
      optionsDialog.getContentPane().add("North", jl);

      JPanel optionsPanel = new JPanel();
      optionsPanel.setLayout(new GridLayout(1, 1));

      JPanel comboPanel = new JPanel();
      comboPanel.setLayout(new GridLayout(0, 1));
      comboPanel.setBorder(new EmptyBorder(20, 20, 20, 20));

      // combobox for the compression method
      compressionMethodComboBox = new JComboBox(
            JrarConstants.addingPannelCompressionStrengths);
      compressionMethodComboBox.setSelectedIndex(currentCompressionMethod);
      compressionMethodComboBox.addActionListener(new ActionListener() {

         public void actionPerformed(ActionEvent e) {
            currentCompressionMethod = compressionMethodComboBox
                  .getSelectedIndex();
         }
      });
      compressionMethodComboBox.setBorder(new TitledBorder(new EtchedBorder(
            EtchedBorder.LOWERED), "Compression Method"));
      compressionMethodComboBox.setToolTipText("Comperssion Method");
      comboPanel.add(compressionMethodComboBox);

      comboPanel.add(Box.createRigidArea(new Dimension(1, 1)));

      // text field for the spliting of archives
      final NumberFormat nf = NumberFormat.getNumberInstance();
      nf.setMaximumIntegerDigits(10);
      splitArchiveTextField = new JFormattedTextField(nf);
      splitArchiveTextField.setColumns(10);
      splitArchiveTextField.setValue(new Double(actualSplitSize));
      splitArchiveTextField.addPropertyChangeListener("value",
            new PropertyChangeListener() {

               public void propertyChange(PropertyChangeEvent e) {
                  actualSplitSize = new Double(splitArchiveTextField.getValue()
                        .toString()).doubleValue();
                  if (actualSplitSize > JrarConstants.maxFileSplitingSize)
                     actualSplitSize = JrarConstants.maxFileSplitingSize;
               }
            });
      splitArchiveTextField.setToolTipText("<html>Split to volumes,"
            + " bytes<br>Enter 0 to enable Auto-Spliting</html>");

      JPanel textFieldPanel = new JPanel();
      textFieldPanel.add(splitArchiveTextField);
      textFieldPanel.setLayout(new GridLayout(1, 1));
      textFieldPanel.setBorder(new TitledBorder(new EtchedBorder(
            EtchedBorder.LOWERED), "Split to volumes, bytes"));
      comboPanel.add(textFieldPanel);

      // checkbox list
      JPanel checkBoxOptions = new JPanel();
      checkBoxOptions.setLayout(new GridLayout(0, 1));
      checkBoxOptions.setBorder(new TitledBorder(new EtchedBorder(
            EtchedBorder.LOWERED), "Archiving Options"));

      for (int i = 0; i < optionCheckBoxes.length; i++) {
         optionCheckBoxes[i] = new JCheckBox();
         optionCheckBoxes[i]
               .setText(JrarConstants.addingPannelCheckBoxOptionTexts[i]);
         optionCheckBoxes[i].addItemListener(this);
         optionCheckBoxes[i].setSelected(options[i]);
         checkBoxOptions.add(optionCheckBoxes[i]);
      }

      optionCheckBoxes[1].setEnabled(false);
      optionCheckBoxes[2].setEnabled(false);
      optionCheckBoxes[3].setEnabled(false);

      optionsPanel.add(comboPanel);
      optionsPanel.add(checkBoxOptions);

      optionsDialog.getContentPane().add("Center", optionsPanel);

      JButton optionsOK = new JButton("OK");
      optionsOK.addActionListener(new ActionListener() {

         public void actionPerformed(ActionEvent e) {
            optionsDialog.dispose();
         }
      });
      optionsDialog.getRootPane().setDefaultButton(optionsOK);
      optionsDialog.getContentPane().add("South", optionsOK);

      optionsDialog.setResizable(false);
      optionsDialog.pack();
      optionsDialog
            .setLocation(
                  (int) ((Toolkit.getDefaultToolkit().getScreenSize()
                        .getWidth()) - optionsDialog.getSize().getWidth()) / 2,
                  (int) ((Toolkit.getDefaultToolkit().getScreenSize()
                        .getHeight()) - optionsDialog.getSize().getHeight()) / 2);
      optionsDialog.setVisible(true);
   }

   public void itemStateChanged(ItemEvent e) {
      for (int i = 0; i < options.length; i++) {
         options[i] = optionCheckBoxes[i].isSelected();
      }
   }

   private void runFinishedCommand() {
      String switchesToUse = "";
      String filesToAdd = "";

      while (currentFile == null)
         currentFile = jrar.noCurrentRarFileOpen();

      if (!currentFile.equals("???CANCEL_ADDING_FILES???")) {

         currentFile = "\"" + currentFile + "\" ";

         // delete files after archiving
         if (options[0])
            switchesToUse += "-df ";

         // TODO create SFX archive
         //if (options[1]) command += "-df ";

         // TODO create solid archive
         //if (options[2]) command += "-df ";

         // TODO add recovery record
         //if (options[3]) switches += "-rr" + recoveryRecordSize + " ";

         // test files on compliation
         if (options[4])
            switchesToUse += "-t ";

         // lock archive
         if (options[5])
            switchesToUse += "-k ";

         switchesToUse += "-m" + currentCompressionMethod + " ";

         for (int i = 0; i < dlm.getSize(); i++)
            filesToAdd += "\"" + dlm.getElementAt(i).toString() + "\" ";

         jrar.createNewAddingProcess(currentFile, switchesToUse, filesToAdd);

         // jrar.runNewRarCommand("a", switches, files, "", message,
         // jrar.ADDSTATE);
      }
   }
}