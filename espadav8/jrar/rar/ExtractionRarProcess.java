package espadav8.jrar.rar;

import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import espadav8.jrar.Jrar;
import espadav8.jrar.JrarConstants;

/*
 * Copyright (C) 2003-2004 Andrew Smith
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307 USA
 */

public class ExtractionRarProcess extends RarProcess implements Runnable {
   String currentFile, currentDir, filesToExtract;

   boolean shouldIStop = false;

   public ExtractionRarProcess(String f, String fte, String d, Jrar j) {
      currentFile = f;
      filesToExtract = fte;
      currentDir = d;
      jrar = j;
   }

   public void run() {
      jrar.setJGUIButtonsState(false);
      jrar.addCancelButtonToJGUIStatusBar(this);
      jrar.updateJGUIStatusBar("" + JrarConstants.EXTRACTIONMESSAGE);
      try {
         // correctly formatting the command to be run
         String command = JrarConstants.extractionCommand + "\"" + currentFile
               + "\" " + filesToExtract + " " + currentDir;
         // finding the location of the EXE file
         String programLocation = jrar.findRarProgram();
         // if jrar.findRarProgram() returns "???RAR_PROGRAM_NOT_FOUND???"
         // then we know the file isn't around and so we can't run the
         // command.
         
         // TODO Add a return to the GUI so that the user knows that the
         // program can't be found
         if (!programLocation.equals("???RAR_PROGRAM_NOT_FOUND???")) {
            // tell people what command is being ran
            jrar.addToDebugWindow("command being run - " + programLocation
                  + command);
            // run the command
            p = Runtime.getRuntime().exec(programLocation + command);
            jrar.addToDebugWindow("Starting startInputListener");
            startInputListener(new InputStreamReader(p.getErrorStream()));
         }
      } catch (Exception e) {
         jrar.updateJGUIStatusBar("Error T: " + e);
         jrar.addToDebugWindow("Error T: " + e);
         p.destroy();
      }
      if (!shouldIStop) {
         jrar.addToDebugWindow("Finished ExtractionRarProcess");
         jrar.extractionRarProcessFinishedSucessfully();
      } else {
         jrar.addToDebugWindow("ExtractionRarProcess stopped by user");
         p.destroy();
         jrar.extractionRarProcessStopped();
      }
   }

   private void startInputListener(InputStreamReader ipsr) {
      String s = "";
      jrar.addToDebugWindow("in startInputListener");
      try {
         int temp;
         do {
            temp = ipsr.read();
            System.out.print((char) temp);
            s += (char) temp;
            String[] sp = s.split("\n");
            if (sp[sp.length - 1].endsWith("[Q]uit")) {
               jrar.addToDebugWindow("overwrite?");
               temp = ipsr.read();
               s += (char) temp;
               sp = s.split("\n");
               overwriteRequest(sp);
            }
         } while ((temp != -1) && (!shouldIStop));
      } catch (Exception e) {
         jrar.addToDebugWindow("Error PSH: " + e);
      }
      if (shouldIStop) {
         jrar.addToDebugWindow("thread canceled by user");
      }
   }

   private void overwriteRequest(String[] splitOutput) {
      String tempFileName = splitOutput[splitOutput.length - 2].substring(0,
            splitOutput[splitOutput.length - 2].length() - 32);
      char answer = jrar.getOverwriteInput(tempFileName);
      BufferedWriter opw = null;
      try {
         opw = new BufferedWriter(new OutputStreamWriter(p
               .getOutputStream()));
         opw.write(answer);
         opw.newLine();
         opw.close();
      } catch (Exception e) {
         jrar.updateJGUIStatusBar("Error O: " + e);
         jrar.addToDebugWindow("Error O: " + e);
      } finally {
         try {
            if (opw != null)
               opw.close();
         }catch (Exception e) {
            e.printStackTrace();
         }
      }
   }

   public void setStop(boolean b) {
      jrar.addToDebugWindow("setting shouldIStop to " + b);
      shouldIStop = b;
   }
}