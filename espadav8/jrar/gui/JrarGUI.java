package espadav8.jrar.gui;

import java.awt.CardLayout;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.InputStreamReader;
import java.net.URL;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.border.BevelBorder;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableColumn;

import espadav8.jrar.Jrar;
import espadav8.jrar.JrarConstants;
import espadav8.jrar.rar.AddingFilePanel;
import espadav8.jrar.rar.RarFileFilter;
import espadav8.jrar.rar.RarFileTableModel;
import espadav8.jrar.rar.RarProcess;

/*
 * Copyright (C) 2003-2004 Andrew Smith
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307 USA
 */

public class JrarGUI extends JFrame implements ActionListener, ItemListener {

	Jrar jrar;

	JFrame f;

	//JButtons used by the main window
	// viewArchiveButton,
	JButton addToArchiveButton, extractToButton, testArchiveButton,
			deleteFileButton, protectButton, sfxButton, openArchiveButton;

	private JPanel buttonPanel, statusBar, tablePanel;

	// Other random JButtons
	private JButton cancelOperation;

	private String currentFile;

	private String oldFile;

	private String currentDir;

	private int currentState = 0;

	private JTable infoTable;

	private JLabel jl;

	private RarFileTableModel ftm;

	private char overwriteAnswer;

	private JDialog overwriteDialog;

	// JButtons used by the JDialog box to confirm overwritting responce
	 private JButton quitOverwrite, noOverwrite, yesOverwrite, neverOverwrite,
			allOverwrite;

	//  private List runningThreads;
	private JScrollPane scrollPane;

	private AddingFilePanel afp;

	public JrarGUI(Jrar j) {
		currentFile = null;
		oldFile = null;
		currentDir = null;
		jrar = j;
		gui();
	}

	public JrarGUI(Jrar j, String s1, String s2) {
		jrar = j;
		oldFile = s1;
		currentFile = s1;
		currentDir = s2;
		gui();
	}

	private void gui() {
		f = new JFrame();
		jrar.addToDebugWindow("Creating main frame");
		f.setTitle(JrarConstants.PROGRAMTITLE);
		jrar.addToDebugWindow("Setting default close operation");
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		currentState = 0;
		jrar.addToDebugWindow("Calling Jrar.createAndPlaceButtons()");
		createAndPlaceButtons();
		jrar.addToDebugWindow("Calling Jrar.createOutputArea()");
		createOutputArea();
		jrar.addToDebugWindow("Calling Jrar.createStatusBar()");
		createStatusBar();
		jrar
				.addToDebugWindow("Adding rigid area to east and west of main frame");
		f.getContentPane()
				.add("East", Box.createRigidArea(new Dimension(8, 0)));
		f.getContentPane()
				.add("West", Box.createRigidArea(new Dimension(8, 0)));
		jrar.addToDebugWindow("setting main frame resizable to "
				+ JrarConstants.MAINFRAMEISRESIZABLE);
		f.setResizable(JrarConstants.MAINFRAMEISRESIZABLE);
		jrar.addToDebugWindow("Setting default size of main frame to width="
				+ JrarConstants.MAINFRAMESIZE.getWidth() + " height="
				+ JrarConstants.MAINFRAMESIZE.getHeight());
		f.setSize(JrarConstants.MAINFRAMESIZE);
		jrar.addToDebugWindow("Packing main frame");
		f.pack();
		jrar.addToDebugWindow("Centering the main frame");
		f.setLocation((int) ((Toolkit.getDefaultToolkit().getScreenSize()
				.getWidth()) - f.getSize().getWidth()) / 2, (int) ((Toolkit
				.getDefaultToolkit().getScreenSize().getHeight()) - f.getSize()
				.getHeight()) / 2);
		jrar.addToDebugWindow("Setting main frame visiable to "
				+ JrarConstants.MAINFRAMEISVISIABLE);
		f.setVisible(JrarConstants.MAINFRAMEISVISIABLE);

		if (currentFile != null) {
			getFileList();
		}

	}

	private void createAndPlaceButtons() {
		URL imageURL;

		jrar.addToDebugWindow("Loading in all the images or the buttons");
		imageURL = Jrar.class.getResource("images/add.png");
		ImageIcon addFileIcon = new ImageIcon(imageURL);
		imageURL = Jrar.class.getResource("images/delete.png");
		ImageIcon deleteFileIcon = new ImageIcon(imageURL);
		imageURL = Jrar.class.getResource("images/extract.png");
		ImageIcon extractFileIcon = new ImageIcon(imageURL);
		imageURL = Jrar.class.getResource("images/open.png");
		ImageIcon openFileIcon = new ImageIcon(imageURL);
		imageURL = Jrar.class.getResource("images/test.png");
		ImageIcon testFileIcon = new ImageIcon(imageURL);
		imageURL = Jrar.class.getResource("images/protect.png");
		ImageIcon protectFileIcon = new ImageIcon(imageURL);
		imageURL = Jrar.class.getResource("images/sfx.png");
		ImageIcon sfxIcon = new ImageIcon(imageURL);

		jrar.addToDebugWindow("Constructing button panel");
		buttonPanel = new JPanel();

		jrar.addToDebugWindow("Setting dimension of buttons to width,height="
				+ JrarConstants.BUTTONSQUARESIZE);
		Dimension d = new Dimension(JrarConstants.BUTTONSQUARESIZE,
				JrarConstants.BUTTONSQUARESIZE);

		jrar.addToDebugWindow("Creating openArchiveButton");
		openArchiveButton = new JButton(openFileIcon);
		openArchiveButton.addActionListener(this);
		openArchiveButton.setPreferredSize(d);

		openArchiveButton
				.setToolTipText("Open and archive to view it's contents");

		jrar.addToDebugWindow("Creating addToArchiveButton");
		addToArchiveButton = new JButton(addFileIcon);
		addToArchiveButton.addActionListener(this);
		addToArchiveButton.setPreferredSize(d);
		addToArchiveButton
				.setToolTipText("Add files to currently open archive");

		jrar.addToDebugWindow("Creating extractTo Button");
		extractToButton = new JButton(extractFileIcon);
		extractToButton.addActionListener(this);
		extractToButton.setPreferredSize(d);
		extractToButton.setToolTipText("Extract selected files");

		jrar.addToDebugWindow("Creating testArchive Button");
		testArchiveButton = new JButton(testFileIcon);
		testArchiveButton.addActionListener(this);
		testArchiveButton.setPreferredSize(d);
		testArchiveButton.setToolTipText("Test this archives integeraty");
		testArchiveButton.setEnabled(false);

		//debugAddition("Creating viewArchive Button");
		//viewArchiveButton = new JButton("View");
		//viewArchiveButton.addActionListener(this);
		//viewArchiveButton.setPreferredSize(d);
		//viewArchiveButton.setToolTipText("View selected file");

		jrar.addToDebugWindow("Creating deleteFile Button");
		deleteFileButton = new JButton(deleteFileIcon);
		deleteFileButton.addActionListener(this);
		deleteFileButton.setPreferredSize(d);
		deleteFileButton
				.setToolTipText("Delete selected file from the archive");
		deleteFileButton.setEnabled(false);

		jrar.addToDebugWindow("Creating protect Button");
		protectButton = new JButton(protectFileIcon);
		protectButton.addActionListener(this);
		protectButton.setPreferredSize(d);
		protectButton.setToolTipText("Protect the archive from modifications");
		protectButton.setEnabled(false);

		jrar.addToDebugWindow("Creating sfx Button");
		sfxButton = new JButton(sfxIcon);
		sfxButton.addActionListener(this);
		sfxButton.setPreferredSize(d);
		sfxButton.setToolTipText("Create an auto-extracting archive");
		sfxButton.setEnabled(false);

		jrar.addToDebugWindow("Adding buttons to button panel");
		buttonPanel.add(openArchiveButton);
		buttonPanel.add(addToArchiveButton);
		buttonPanel.add(extractToButton);
		buttonPanel.add(testArchiveButton);
		//buttonPanel.add(viewArchiveButton);
		buttonPanel.add(deleteFileButton);
		buttonPanel.add(protectButton);
		buttonPanel.add(sfxButton);

		if (jrar.isDebugOn()) {
			jrar.addToDebugWindow("Creating Debug button");
			imageURL = Jrar.class.getResource("images/debug.png");
			ImageIcon debugIcon = new ImageIcon(imageURL);
			JButton showHideDebugFrame = new JButton(debugIcon);
			//        "<html><center>Show/Hide<br>Debug Window</center></html>");
			showHideDebugFrame.addActionListener(new ActionListener() {

				public void actionPerformed(ActionEvent e) {
					jrar.switchDWVisibilaty();
				}
			});
			showHideDebugFrame.setPreferredSize(new Dimension(120,
					JrarConstants.BUTTONSQUARESIZE));
			buttonPanel.add(showHideDebugFrame);
			jrar
					.addToDebugWindow("Adding button panel to the north of the main frame");
		}
		f.getContentPane().add("North", buttonPanel);
	}

	private void createOutputArea() {
		tablePanel = new JPanel(new CardLayout());
		jrar.addToDebugWindow("Creating a new FileTableModel");
		ftm = new RarFileTableModel();
		jrar.addToDebugWindow("Setting up a table with the created"
				+ "FileTableModel in the constructor");
		infoTable = new JTable(ftm);
		jrar
				.addToDebugWindow("Disabling the auto resize of the tables columns");
		// disables auto sizing of the columns
		infoTable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		jrar.addToDebugWindow("Creating an array to store the tables columns");
		TableColumn columns[] = new TableColumn[infoTable.getColumnCount()];
		jrar
				.addToDebugWindow("Creating an array to store the tables columns renderers");
		DefaultTableCellRenderer[] columnRenderers = new DefaultTableCellRenderer[infoTable
				.getColumnCount()];
		jrar
				.addToDebugWindow("Setting up each column and column renderer for the table");
		for (int i = 0; i < infoTable.getColumnCount(); i++) {
			columns[i] = infoTable.getColumn(infoTable.getColumnName(i));
			columnRenderers[i] = new DefaultTableCellRenderer();
		}
		jrar.addToDebugWindow("Configuring the name column");
		// Name column
		columnRenderers[0].setHorizontalAlignment(SwingConstants.LEFT);
		columns[0].setCellRenderer(columnRenderers[0]);
		columns[0].setPreferredWidth(196);
		jrar.addToDebugWindow("Configuring the original size column");
		// original size
		columnRenderers[1].setHorizontalAlignment(SwingConstants.RIGHT);
		columns[1].setCellRenderer(columnRenderers[1]);
		columns[1].setPreferredWidth(80);
		jrar.addToDebugWindow("Configuring the packed size column");
		// packed size
		columnRenderers[2].setHorizontalAlignment(SwingConstants.RIGHT);
		columns[2].setCellRenderer(columnRenderers[2]);
		columns[2].setPreferredWidth(80);
		jrar.addToDebugWindow("Configuring the ratio column");
		// ratio
		columnRenderers[3].setHorizontalAlignment(SwingConstants.RIGHT);
		columns[3].setCellRenderer(columnRenderers[3]);
		columns[3].setPreferredWidth(50);
		jrar.addToDebugWindow("Configuring the data column");
		// data
		columnRenderers[4].setHorizontalAlignment(SwingConstants.CENTER);
		columns[4].setCellRenderer(columnRenderers[4]);
		columns[4].setPreferredWidth(60);
		jrar.addToDebugWindow("Configuring the time column");
		// time
		columnRenderers[5].setHorizontalAlignment(SwingConstants.CENTER);
		columns[5].setCellRenderer(columnRenderers[5]);
		columns[5].setPreferredWidth(50);
		jrar.addToDebugWindow("Configuring the attributes column");
		// attributes
		columnRenderers[6].setHorizontalAlignment(SwingConstants.CENTER);
		columns[6].setCellRenderer(columnRenderers[6]);
		columns[6].setPreferredWidth(60);
		jrar.addToDebugWindow("Configuring the crc32 column");
		// crc32
		columnRenderers[7].setHorizontalAlignment(SwingConstants.CENTER);
		columns[7].setCellRenderer(columnRenderers[7]);
		columns[7].setPreferredWidth(70);
		jrar.addToDebugWindow("Creating a scrollpane with the table"
				+ "passed into the constructor");
		scrollPane = new JScrollPane(infoTable);
		scrollPane.setBorder(new BevelBorder(BevelBorder.LOWERED));
		scrollPane.setPreferredSize(new Dimension(600, 500));
		scrollPane
				.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		jrar
				.addToDebugWindow("Adding the scrollpane to the center of the main frame");
		tablePanel.add(scrollPane, "File List");
		f.getContentPane().add("Center", tablePanel);
		//this.getContentPane().add("Center", scrollPane);
	}

	private void createStatusBar() {
		jrar
				.addToDebugWindow("Creating the status bar panel and giving it a Box layout");
		statusBar = new JPanel();
		statusBar.setLayout(new BoxLayout(statusBar, BoxLayout.LINE_AXIS));
		jrar.addToDebugWindow("Creating the JLabel for the status bar");
		jl = new JLabel("Finished Loading");
		jrar
				.addToDebugWindow("Adding a small rigid box to the status bar panel");
		statusBar.add(Box.createRigidArea(new Dimension(10, 0)));
		jrar.addToDebugWindow("Adding the JLabel to the statud bar panel");
		statusBar.add(jl);
		jrar
				.addToDebugWindow("Adding the status bar to the South of the main frame");
		f.getContentPane().add("South", statusBar);
	}

	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == openArchiveButton) {
			jrar
					.addToDebugWindow("Open arcive button pressed, calling Jrar.openFileChooser()");
			openFileChooser();
		} else if (e.getSource() == addToArchiveButton) {
			jrar
					.addToDebugWindow("Add button pressed, calling Jrar.addFilesToArchive()");
			addFilesToArchive();
		} else if (e.getSource() == extractToButton) {
			jrar
					.addToDebugWindow("Extract To button pressed, calling Jrar.extractFilesTo()");
			extractFilesTo();
		} else if (e.getSource() == sfxButton) {
			jrar
					.addToDebugWindow("SFX button pressed, calling <NOTHING CALLED YET>");
		} else if (e.getSource() == testArchiveButton) {
			jrar
					.addToDebugWindow("Test Archive button pressed, calling Jrar.testArchive()");
			      // testArchive();
		} else if (e.getSource() == yesOverwrite) {
			jrar
					.addToDebugWindow("Overwrite \'Yes\' button pressed, overwriteAnswer set to "
							+ JrarConstants.OVERWRITEYESANSWER);
			overwriteAnswer = JrarConstants.OVERWRITEYESANSWER;
			overwriteDialog.dispose();
		} else if (e.getSource() == noOverwrite) {
			jrar
					.addToDebugWindow("Overwrite \'No\' button pressed, overwriteAnswer set to "
							+ JrarConstants.OVERWRITENOANSWER);
			overwriteAnswer = JrarConstants.OVERWRITENOANSWER;
			overwriteDialog.dispose();
		} else if (e.getSource() == allOverwrite) {
			jrar
					.addToDebugWindow("Overwrite \'All\' button pressed, overwriteAnswer set to "
							+ JrarConstants.OVERWRITEALLANSWER);
			overwriteAnswer = JrarConstants.OVERWRITEALLANSWER;
			overwriteDialog.dispose();
		} else if (e.getSource() == neverOverwrite) {
			jrar
					.addToDebugWindow("Overwrite \'Never\' button pressed, overwriteAnswer set to "
							+ JrarConstants.OVERWRITENEVERANSWER);
			overwriteAnswer = JrarConstants.OVERWRITENEVERANSWER;
			overwriteDialog.dispose();
		} else if (e.getSource() == quitOverwrite) {
			jrar
					.addToDebugWindow("Overwrite \'Quit\' button pressed, overwriteAnswer set to "
							+ JrarConstants.OVERWRITEQUITANSWER);
			overwriteAnswer = JrarConstants.OVERWRITEQUITANSWER;
			overwriteDialog.dispose();
		}
	}

	private void openFileChooser() {
		JFileChooser fc = new JFileChooser();
		fc.setFileFilter(new RarFileFilter());
		int answer = fc.showOpenDialog(this);
		if (answer == JFileChooser.APPROVE_OPTION) {
			oldFile = currentFile;
			ftm.resetToNewData();
			ftm.fireTableDataChanged();
			try {
				currentFile = fc.getSelectedFile().getCanonicalPath();
				getFileList();
			} catch (Exception exp) {
				updateStatusBar("Error 3: " + exp);
			}
		}
	}

	private void getFileList() {
		String op = "";
		String command = jrar.findRarProgram();
		if (command.equals(JrarConstants.rarProgramNotFound)) {
			jrar.addToDebugWindow("RAR program not found, can't open file");
			updateStatusBar("RAR program not found, can't open file");
			//displayErrorDialog("RAR program not found, can't open file");
		} else {
		   InputStreamReader berr = null;
			try {
				Process p = Runtime.getRuntime().exec(
						"rar vt  \"" + currentFile + "\"");
				berr = new InputStreamReader(p
						.getInputStream());
				int temp = berr.read();
				do {
					op += (char) temp;
					temp = berr.read();
				} while (temp != -1);
				berr.close();
			} catch (Exception ex) {
				updateStatusBar("Error 1: " + ex);
			} finally {
			   try {
			      if (berr != null)
			         berr.close();
			   } catch (Exception e) {
			      e.printStackTrace();
			   }
			}
			//  split the output into seperate strings after each new line
			String[] stringSplit = op.split("\\n");
			// trim all the whitespace off the lines
			for (int i = 0; i < stringSplit.length; i++)
				stringSplit[i] = stringSplit[i].trim();
			if (stringSplit[stringSplit.length - 1]
					.endsWith(".rar is not RAR archive")) {
				if (oldFile != null) {
					String of2 = currentFile;
					currentFile = oldFile;
					getFileList();
					updateStatusBar(of2 + " is not a RAR archive or is damaged");
				} else {
					updateStatusBar(currentFile
							+ " is not a RAR archive or is damaged");
				}
			} else {
				// create this to remove the leading junk we don't want
				String[] temp = new String[stringSplit.length - 11];
				// copy everything over except the first 10 lines and the last
				// line
				for (int i = 0; i < stringSplit.length - 11; i++)
					temp[i] = stringSplit[i + 10];
				// set stringSplit to the updated version
				stringSplit = temp;
				// the real work starts here for every file there are 3 lines we
				// need to
				// get the data from. The last 2 lines of the stringSplit array
				// are
				// info
				// on the whole archive and we don't need to worry about those
				// yet
				// so we
				// take those off the length and then divide the total by 3
				// resulting in
				// the number of files in the archive.
				for (int i = 0; i < (stringSplit.length - 2) / 3; i++) {
					// the first thing we do it get the file name, this is the
					// first line
					// of the 3 we're looking at with indices 0, 3, 6, 9... and
					// so
					// on
					String nextFileName = stringSplit[i * 3].trim();
					// System.out.println(nextFileName);
					// the next line in the list is the contatins a lot of info
					// we
					// need as
					// well as a lot of whitespace. we pass this into
					// formatFileList which
					// removes the whitespace and returns a nice array for us
					String[] info = formatFileList(stringSplit[(i * 3) + 1]
							.split(" "), "", 9);
					// the third line has some techinal info. This isn't used at
					// the moment
					// but we store it now for use later on
					String[] extraInfo = formatFileList(
							stringSplit[(i * 3) + 2].split(" "), "", 3);
					// combine the three things above to make it easier to work
					// with in the
					// table model
					String[] newRow = combineStringArrays(nextFileName, info,
							extraInfo);
					// add the new row to the data in myTableModel
					ftm.addNewRow(newRow);
					// and fire a data change
					ftm.fireTableDataChanged();
				}
				// next we want to get the archive info and update the status
				// bar
				String[] archiveInfo = formatFileList(
						stringSplit[stringSplit.length - 1].split(" "), "", 4);
				updateStatusBar("Number of Files: " + archiveInfo[0]
						+ " Total Size: " + archiveInfo[1]
						+ " Compressed Size: " + archiveInfo[2] + " Ratio: "
						+ archiveInfo[3]);
			}
		}
	}

	private String[] combineStringArrays(String s1, String[] s2, String[] s3) {
		String[] s4 = new String[1 + s2.length + s3.length];
		s4[0] = s1;
		for (int i = 0; i < s2.length; i++) {
			s4[i + 1] = s2[i];
		}
		for (int i = 0; i < s3.length; i++) {
			s4[i + s2.length] = s3[i];
		}
		return s4;
	}

	private String[] formatFileList(String[] a, String s, int x) {
		String[] temp = new String[x];
		int j = 0;
		for (int i = 0; i < a.length; i++) {
			if (a[i].equals(s)) {
			} else {
				temp[j] = a[i].trim();
				j++;
			}
		}
		a = temp;
		return a;
	}

	public void updateStatusBar(String s) {
		jl.setText(s);
	}

	public void itemStateChanged(ItemEvent e) {
		//      TODO
	}

	private void addFilesToArchive() {
		setButtonsState(false);
		afp = new AddingFilePanel(tablePanel, jrar, currentFile);
		tablePanel.add(afp, "Adding Card");
		CardLayout cl = (CardLayout) (tablePanel.getLayout());
		cl.last(tablePanel);
	}

	public void setButtonsState(boolean b) {
		openArchiveButton.setEnabled(b);
		addToArchiveButton.setEnabled(b);
		extractToButton.setEnabled(b);
		//testArchiveButton.setEnabled(b);
		//viewArchiveButton.setEnabled(b);
		//deleteFileButton.setEnabled(b);
		//protectButton.setEnabled(b);
		//sfxButton.setEnabled(b);
	}

	//  TODO
	//private void createNewArchivePopup() { }

	public void debuggingStopped() {
		buttonPanel.remove(buttonPanel.getComponent(buttonPanel
				.getComponentCount() - 1));
		buttonPanel.repaint();
		f.pack();
		f.setLocation((int) ((Toolkit.getDefaultToolkit().getScreenSize()
				.getWidth()) - f.getSize().getWidth()) / 2, (int) ((Toolkit
				.getDefaultToolkit().getScreenSize().getHeight()) - f.getSize()
				.getHeight()) / 2);
	}

	private void extractFilesTo() {
		if (currentFile != null) {
			JFileChooser fc = new JFileChooser();
			fc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
			int answer = fc.showDialog(this, "Extract here");
			if (answer == JFileChooser.APPROVE_OPTION) {
				try {
					currentDir = "\"" + fc.getSelectedFile().getCanonicalPath()
							+ "\"";
				} catch (Exception exp) {
					updateStatusBar("Error 5: " + exp);
				}
				String selectedFiles = "";
				if (infoTable.getSelectedRowCount() != 0) {
					jrar.addToDebugWindow("" + infoTable.getSelectedRowCount());
					int[] filesToExtract = infoTable.getSelectedRows();
					for (int i = 0; i < (filesToExtract.length); i++) {
						selectedFiles += "\""
								+ infoTable.getValueAt(filesToExtract[i], 0)
										.toString() + "\" ";
						jrar.addToDebugWindow(selectedFiles);
					}
					selectedFiles = selectedFiles.trim();
				} else if (infoTable.getSelectedRowCount() == 0) {
					for (int i = 0; i < infoTable.getRowCount(); i++) {
						selectedFiles += "\""
								+ infoTable.getValueAt(i, 0).toString() + "\" ";
					}
				}
				jrar.createNewExtractionProcess(currentFile, selectedFiles,
						currentDir);
			}
		} else {
			updateStatusBar("No open RAR file");
		}
	}

	public void extractionRarProcessFinishedSucessfully() {
		updateStatusBar("Extraction complete");
		setButtonsState(true);
		statusBar.remove(cancelOperation);
		statusBar.repaint();
	}

	public void addingRarProcessFinishedSucessfully() {
		updateStatusBar("Addition complete");
		setButtonsState(true);
		statusBar.remove(cancelOperation);
		statusBar.repaint();
		CardLayout cl = (CardLayout) (tablePanel.getLayout());
		tablePanel.remove(afp);
		cl.removeLayoutComponent(afp);
		cl.last(tablePanel);
		ftm.resetToNewData();
		getFileList();
	}

	public void addCancelButtonToStatusBar(final RarProcess rp) {
		cancelOperation = new JButton("Cancel");
		statusBar.add(cancelOperation);
		cancelOperation.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				jrar.addToDebugWindow("Cancel button pressed");
				jrar.addToDebugWindow("Stopping process");
				rp.setStop(true);
				jrar.addToDebugWindow("Removing Cancel button from status bar");
				statusBar.remove(cancelOperation);
				jrar.addToDebugWindow("Validating status bar to update screen");
				setButtonsState(true);
				statusBar.revalidate();
			}
		});
		statusBar.validate();
		statusBar.repaint();
	}

	public String newRarFilePopup() {
		JFileChooser fc = new JFileChooser();
		fc.setMultiSelectionEnabled(false);
		fc.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
		fc.setFileFilter(new RarFileFilter());
		int answer = fc.showDialog(this,
				"Create new RAR file or add to existing one");

		if (answer == JFileChooser.APPROVE_OPTION) {
			String saveFile = fc.getSelectedFile().toString();
			if (saveFile.endsWith(".rar"))
				currentFile = saveFile;
			else
				currentFile = saveFile + ".rar";
			System.out.println("saveFile = " + saveFile);
			System.out.println("currentFile = " + currentFile);
		}

		else if (answer == JFileChooser.CANCEL_OPTION) {
			currentFile = "???CANCEL_ADDING_FILES???";
		}
		jrar.addToDebugWindow("Adding files to " + currentFile);
		return currentFile;
	}
}