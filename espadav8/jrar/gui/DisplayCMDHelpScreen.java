package espadav8.jrar.gui;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;

/*
 * Copyright (C) 2003-2004 Andrew Smith
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307 USA
 */

public class DisplayCMDHelpScreen extends JFrame {

   public DisplayCMDHelpScreen() {
      System.out.println("Help mode");
      final JFrame f = new JFrame("Command Line options");
      f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
      String help = "<html>The following commands are available<br><br>"
            + "/?, -h, --help&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;display this help box<br>"
            + "--debug&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;enable debugging mode"
            + "<br>&nbsp;</html>";
      JLabel jl = new JLabel(help);
      f.getContentPane().add("Center", jl);
      JButton jb = new JButton("Close");
      jb.addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent e) {
            f.dispose();
            System.exit(0);
         }
      });
      f.getContentPane().add("South", jb);
      f.getContentPane().add("North",
            Box.createRigidArea(new Dimension(10, 10)));
      f.getContentPane()
            .add("East", Box.createRigidArea(new Dimension(10, 10)));
      f.getContentPane()
            .add("West", Box.createRigidArea(new Dimension(10, 10)));

      f.pack();

      f.setLocation((int) ((Toolkit.getDefaultToolkit().getScreenSize()
            .getWidth()) - f.getSize().getWidth()) / 2, (int) ((Toolkit
            .getDefaultToolkit().getScreenSize().getHeight()) - f.getSize()
            .getHeight()) / 2);

      f.setResizable(false);
      f.setVisible(true);
   }
}