package espadav8.jrar;

import java.io.File;

import espadav8.jrar.debug.DebugWindow;
import espadav8.jrar.gui.DisplayCMDHelpScreen;
import espadav8.jrar.gui.JrarGUI;
import espadav8.jrar.rar.AddingRarProcess;
import espadav8.jrar.rar.ExtractionRarProcess;
import espadav8.jrar.rar.RarProcess;

/*
 * Copyright (C) 2003-2004 Andrew Smith
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307 USA
 */

public class Jrar {
   private static boolean DISPLAYHELP = false;

   private static boolean DEBUG = true;

   private static DebugWindow dw;

   private static JrarGUI jgui;

   public static void main(String[] args) {
      if (args.length > 0) {
         for (int i = 0; i < args.length; i++) {
            if ((args[i].equals("--help"))
                  || (args[i].equals("-h") || args[i].equals("/?")))
               DISPLAYHELP = true;
            else if (args[i].equals("--debug"))
               DEBUG = true;
         }
      }
      new Jrar();
   }

   public Jrar() {
      if (!DISPLAYHELP) {
         if (DEBUG)
            dw = new DebugWindow(this);
         jgui = new JrarGUI(this);
      } else {
         new DisplayCMDHelpScreen();
      }
   }

   public void stopDebugging() {
      dw.dispose();
      jgui.debuggingStopped();
      DEBUG = false;
   }

   public void addToDebugWindow(String s) {
      if (DEBUG)
         dw.addDebugOutput(s);
   }

   public boolean isDebugOn() {
      return DEBUG;
   }

   public void switchDWVisibilaty() {
      addToDebugWindow("Switching debug window visiability");
      dw.debugFrameVisiability();
   }

   public void createNewAddingProcess(String currentFile, String switchesToUse,
         String filesToAdd) {
      addToDebugWindow("Creating a new RAR process to add the files");
      AddingRarProcess arp = new AddingRarProcess(currentFile, switchesToUse,
            filesToAdd, this);
      arp.start();
   }

   public void updateJGUIStatusBar(String s) {
      addToDebugWindow("Calling jgui to update the statusbar");
      jgui.updateStatusBar(s);
   }

   public void setJGUIButtonsState(boolean b) {
      addToDebugWindow("Calling jgui to set the status of the GUI buttons");
      jgui.setButtonsState(b);
   }

   public void extractionRarProcessFinishedSucessfully() {
      addToDebugWindow("Extraction Process completed sucessfully");
      jgui.extractionRarProcessFinishedSucessfully();
   }

   public char getOverwriteInput(String tempFileName) {
      //	TODO
      return 'c';
   }

   public void createNewExtractionProcess(String rarFile,
         String filesToExtract, String dir) {
      ExtractionRarProcess erp = new ExtractionRarProcess(rarFile,
            filesToExtract, dir, this);
      erp.start();
   }

   public void addCancelButtonToJGUIStatusBar(RarProcess rp) {
      addToDebugWindow("Adding a cancel button to the status bar for the current process");
      //	TODO addCancelButtonToJGUIStatusBar
      jgui.addCancelButtonToStatusBar(rp);
   }

   public void addingRarProcessFinishedSucessfully() {
      // TODO Auto-generated method stub
      addToDebugWindow("Adding process completed sucessfully");
      jgui.addingRarProcessFinishedSucessfully();
   }

   public void extractionRarProcessStopped() {
      // TODO Auto-generated method stub
   }

   public void addingRarProcessStopped() {
      // TODO Auto-generated method stub
   }

   public String noCurrentRarFileOpen() {
      addToDebugWindow("Creating a new window to get the name of the new file");
      return jgui.newRarFilePopup();
   }

   public String findRarProgram() {

      addToDebugWindow("Finding the location of the RAR program");

      addToDebugWindow("Getting the system info needed");
      String PATH_STRING = System.getProperty("java.library.path");
      String PATH_SEPARATOR = System.getProperty("path.separator");
      String[] SPLIT_PATH_STRING = PATH_STRING.split(PATH_SEPARATOR);
      String FILE_SEPARATOR = System.getProperty("file.separator");
      String PROGRAM_NAME = "";

      // makes sure that each PATH ends in the FILE_SEPARATOR char
      for (int i = 0; i < SPLIT_PATH_STRING.length; i++) {
         if (!SPLIT_PATH_STRING[i].endsWith(FILE_SEPARATOR)) {
            SPLIT_PATH_STRING[i] += FILE_SEPARATOR;
         }
      }
      // runs through each PATH location and checks for the file rar or
      // rar.exe
      for (int i = 0; i < SPLIT_PATH_STRING.length; i++) {
         String location = SPLIT_PATH_STRING[i];
         System.out.println(location);
         if ((new File(location + "rar").exists())
               || (new File(location + "rar.exe").exists())) {
            System.out.println("file found");
            if (isDebugOn())
               addToDebugWindow("file found at " + location);
            return location;
         } else if (isDebugOn()) {
            addToDebugWindow("file NOT found at " + location);
         }
      }
      return JrarConstants.rarProgramNotFound;
   }
}